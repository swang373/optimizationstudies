# Optimization Studies

The goal of this repository is to provide the analysis team a place to document the results of their optimization studies and share reproducible notebooks or scripts. The idea is to organize studies into subdirectories which contain:

* A setup script which configures the environment and installs dependencies.
* Any custom user code needed such as utility functions and custom libraries.
* A minimally working example script and accompanying documentation or a Jupyter notebook which presents the code inline with documentation.
* Any data files needed to provide a working example. If these are too large, they should be shared as needed or placed in a public area rather than committed to the repository.

Analysis team members should feel free to contribute as much as they would like, from expanding the content of existing subdirectories to creating new subdirectories as new studies and ideas arise.

## Installation

The best user experience is on interactive nodes, such as `lxplus` or `cmslpc`, which have ssh tunneling enabled for Jupyter access and HTCondor for batch compute. However, users are not constrained to use the repository in this manner, as the notebooks and results should be rendered by GitLab for viewing using a web browser.

The easiest way to ensure most dependencies are available, such as ROOT and various Python packages, is to clone the repository within a CMSSW release. For the instructions below, the repository is installed within CMSSW\_9\_4\_4, though it should work for newer versions of CMSSW as well. Your mileage may vary for older releases.

```bash
# Setup CMSSW
export SCRAM_ARCH="slc6_amd64_gcc630"
scram project CMSSW CMSSW_9_4_4
cd CMSSW_9_4_4/src
cmsenv

# Clone the repository and install the autocategorizer
git clone <ssh or html repo link>
cd OptimizationStudies
```

## Tunneling to a Jupyter Server

Jupyter is distributed alongside modern CMSSW versions, enabling users to develop their code within notebooks. The commands to setup an ssh tunnel and connect to a Jupyter server are for `lxplus`:

```bash
# ssh with tunneling
ssh -L localhost:8888:localhost:8888 <username>@lxplus.cern.ch

# Setup a CMSSW environment and start a Jupyter server
cd CMSSW_X_Y_Z/src
cmsenv
jupyter notebook --no-browser --port=8888 --ip=127.0.0.1
```

and similarly for `cmslpc`:

```bash
# Assuming you have Kerberos ticket, ssh with tunneling
ssh -L localhost:8888:localhost:8888 <username>@cmslpc-sl6.fnal.gov

# Setup a CMSSW environment and start a Jupyter server
cd CMSSW_X_Y_Z/src
cmsenv
jupyter notebook --no-browser --port=8888 --ip=127.0.0.1
```

The default port number of Jupyter servers is 8888, so if it is unavailable because it is already in use, just try a different number.

