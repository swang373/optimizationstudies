#!/usr/bin/env python
import collections
import glob
import os
import uuid
import sys
import warnings

import ROOT
import numpy


################################################################################
# TEMPLATE CONSTANTS

# The name of the TTree in the ntuple.
TREE_NAME = 'Events'

# The list of cuts defining the signal region.
SIGNAL_REGION = [
{% for cut in signal_region %}
    '{{ cut }}',
{% endfor %}
]

# The list of branches to drop from the tree.
DROPPED_BRANCHES = [
{% for branch in drop_branches %}
    '{{ branch }}',
{% endfor %}
]

# The dictionary of new branches and their TTreeFormulas.
NEW_BRANCHES = collections.OrderedDict()
{% for branch_exp, formula_exp in new_branches.iteritems() %}
NEW_BRANCHES['{{ branch_exp }}'] = '{{ formula_exp }}'
{% endfor %}

################################################################################


class Events(ROOT.TChain):
    """A TChain subclass with convenience methods for preprocessing.

    Assumes the name of the tree is "tree" as in Heppy ntuples. This will have
    to be updated to "Events" upon transition to NanoAOD ntuples.

    Parameters
    ----------
    filenames : paths
        An iterable of input file paths.
    """
    def __init__(self, filenames, tree_name):
        super(Events, self).__init__(tree_name)
        for filename in filenames:
            self.Add(filename)

    def __len__(self):
        """The total number of entries."""
        return self.GetEntries()

    def _create_eventlist(self, selection):
        """Return the eventlist for a selection."""
        name = uuid.uuid4().hex
        self.Draw('>>{0}'.format(name), selection, 'goff')
        eventlist = ROOT.gDirectory.Get(name)
        eventlist.SetDirectory(0)
        return eventlist

    def deactivate(self, *branches):
        """Deactivate branches.

        Parameters
        ----------
        *branches : strings
            The names of the branches to deactivate. Wildcarded TRegexp
            expressions such as "a*b" or "a*b*" are also valid. Unrecognized
            branches are ignored.
        """
        # The "found" argument which is used to suppress warnings/errors
        # about unknown branches must be passed as an unsigned int.
        found = numpy.array([1], dtype=numpy.uint32)
        for branch in branches:
            self.SetBranchStatus(branch, 0, found)

    def select(self, *selections):
        """Return the eventlist for selections on the events.

        Parameters
        ----------
        *selections : strings
            The selection expressions.
        """
        eventlist = self._create_eventlist(selections[0])
        for selection in selections[1:]:
            eventlist.Intersect(self._create_eventlist(selection))
        return eventlist


def main():
    # Catch a specific ROOT warning related to TTreeFormulas.
    # See https://root-forum.cern.ch/t/creating-converter-when-using-ttreeformula/13845/3
    warnings.filterwarnings(action='ignore', category=RuntimeWarning, message='creating converter.*')

    print 'Loading ROOT macros'
    macros = glob.glob('*.C')
    for macro in macros:
        if ROOT.gROOT.LoadMacro(macro + '+') == -1:
            raise RuntimeError('Unable to load macro {0}'.format(macro))

    print 'Determining signal region events and dropping branches'
    events = Events(filenames=sys.argv[1:-1], tree_name=TREE_NAME)
    eventlist = events.select(*SIGNAL_REGION)
    signal_region = set(eventlist.GetEntry(i) for i in xrange(eventlist.GetN()))
    events.deactivate(*DROPPED_BRANCHES)

    print 'Generating temporary tree'
    tmp = ROOT.TFile.Open('tmp.root', 'recreate')
    tree_tmp = events.CopyTree('')
    tmp.Write()
    tmp.Flush()

    print 'Creating output tree and setting up new branches'
    out = ROOT.TFile.Open(sys.argv[-1], 'recreate')
    tree_out = tree_tmp.CloneTree(0)

    addresses, formulas, root_types = {}, {}, {'I': numpy.int32, 'F': numpy.float32}
    for i, (branch_exp, formula_exp) in enumerate(NEW_BRANCHES.iteritems()):
        name, dtype = branch_exp.split('/')
        addresses[name] = numpy.array([0], dtype=root_types[dtype])
        formulas[name] = ROOT.TTreeFormula(name, formula_exp, tree_tmp)
        tree_out.Branch(name, addresses[name], branch_exp)

    for i, _ in enumerate(tree_tmp):
        if i % 1000 == 0:
            print 'Filling Event {0!s}'.format(i)
        for name, formula in formulas.iteritems():
            formula.UpdateFormulaLeaves()
            formula.GetNdata()
            addresses[name][0] = formula.EvalInstance()
            if i not in signal_region:
                addresses['bin'][0] = -1
        tree_out.Fill()

    print 'Finished filling output tree with {0!s} total events'.format(tree_out.GetEntries())
    out.Write()
    out.Close()

    print 'Cleaning up temporary tree'
    tmp.Close()
    os.remove('tmp.root')


if __name__ == '__main__':

    status = main()
    sys.exit(status)

