import errno
import os
import xrootdpyfs


def safe_makedirs(path):
    """Recursively create a directory without race conditions.

    This borrows from solutions to these Stack Overflow questions:
    * http://stackoverflow.com/a/5032238
    * http://stackoverflow.com/a/600612

    Parameters
    ----------
    path : path
        The path of the created directory.
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def xrootd_makedirs(url):
    """Recursively create an XRootD directory.

    Parameters
    ----------
    url : URL
        The XRootD URL for the created directory. The protocol
        and the base path must be separated by a double slash "//".
    """
    fs = xrootdpyfs.XRootDPyFS(url)
    return fs.makedir('', recursive=True, allow_recreate=True)


def xrootd_find_files(url):
    """Find all ROOT files located under an XRootD directory.

    Care should be taken when choosing the depth of the
    base search directory because this is a recursive search.

    Parameters
    ----------
    url : URL
        The XRootD URL for the base search directory. The protocol
        and the base path must be separated by a double slash "//".

    Returns
    -------
    urls : list of URLS
        The sorted list of XRootD URLS for the located files.
    """
    fs = xrootdpyfs.XRootDPyFS(url)
    root_url = fs.root_url
    if fs.isdir(fs.base_path):
        filepaths = fs.walkfiles(fs.base_path)
        urls = sorted('{0}/{1}'.format(root_url, filepath) for filepath in filepaths if filepath.endswith('.root'))
        return urls
    else:
        return ['{0}/{1}'.format(root_url, fs.base_path)]

