from __future__ import absolute_import

import collections
import os
import re
import shutil
import subprocess
import time

import numpy

from .dag import DAG
from .utils import safe_makedirs, xrootd_find_files, xrootd_makedirs


class PreprocessorError(Exception):
    pass


class Preprocessor(DAG):
    """An interface for submitting preprocessing jobs to HTCondor.

    Parameters
    ----------
    input_files : path or iterable of path, optional
        The path or paths for additional input files provided by the user.
    no_submit : bool, optional
        If true, prevents the submit method from submitting jobs
        to the scheduler. The default is False.
    """
    def __init__(self, input_files=[], no_submit=False):
        super(Preprocessor, self).__init__(input_files)
        self.no_submit = no_submit
        self._bin_edges = None
        self.bin_variable = None
        self._signal_region = []
        self._new_branches = collections.OrderedDict([('weight/F', '1')])
        self._drop_branches = []

    @property
    def bin_edges(self):
        return self._bin_edges

    @bin_edges.setter
    def bin_edges(self, value):
        if isinstance(value, basestring):
            try:
                x_min, x_max, n_bins = [int(_) for _ in re.split(r'\s*,\s*', value)]
            except Exception:
                raise PreprocessorError('The string describing the binning must use the format "x_min, x_max, n_bins".')
            self._bin_edges = list(numpy.linspace(start=x_min, stop=x_max, num=n_bins, endpoint=False))
            self._bin_edges.append(x_max)
        else:
            self._bin_edges = list(value)

    @property
    def signal_region(self):
        return self._signal_region

    @signal_region.setter
    def signal_region(self, value):
        if isinstance(value, basestring):
            self._signal_region = [value]
        else:
            self._signal_region = list(value)

    @property
    def new_branches(self):
        return self._new_branches

    @property
    def drop_branches(self):
        return self._drop_branches

    @drop_branches.setter
    def drop_branches(self, value):
        if isinstance(value, basestring):
            self._drop_branches = [value]
        else:
            self._drop_branches = list(value)

    def _format_bin_index_formula(self):
        subformula = '{1!s} * ({0} > {2!s} && {0} <= {3!s})'
        bin_index_formula = ' + '.join([subformula.format(self.bin_variable, i, self.bin_edges[i], self.bin_edges[i+1]) for i in xrange(len(self.bin_edges) - 1)])
        return bin_index_formula

    def submit(self, name, src, dst, lumi_scale=None, scale_factor=None, is_signal=False, commands={}, max_jobs=250, no_submit=False):
        """Submit preprocessing jobs to HTCondor DAGMan.

        DAGMan jobs can be retried automatically and, should jobs fail, users can
        take advantage of the automatically generated rescue DAG for resubmitting
        only failed jobs.

        For more information, see the DAGMan documentation at
        http://research.cs.wisc.edu/htcondor/manual/latest/2_10DAGMan_Applications.html

        Parameters
        ----------
        name : string
            The name of the parent directory for the generated job submission files.
        src : url
            The XRootD url of the directory containing the input ntuples.
            Any .root files are automatically located by recursing through subdirectories.
        dst : url
            The XRootD url of the directory containing the output ROOT files.
            By convention, the url needs to end with a trailing separator, e.g. "/".
        lumi_scale : numeric, optional
            The factor used to scale the Monte-Carlo sample to a target luminosity,
            typically derived using the sample's count histograms and cross section.
        scale_factor : string, optional
            Any additional scale factors applied to the specific Monte-Carlo sample.
        is_signal : bool, optional
            Whether the Monte-Carlo sample is signal or background. The default is
            False for background.
        commands : dict, optional
            HTCondor commands to include in the submit description file, in addition to the
            following which are handled automatically:
                * arguments
                * error
                * executable
                * getenv
                * log
                * output
                * queue
                * should_transfer_files
                * transfer_input_files
                * transfer_output_files
                * universe
            The default is no additional commands.
        max_jobs : int, optional
            The maximum number of concurrent jobs within the DAG. The default is 250.
        no_submit : bool, optional
            If True, the job submission files are generated but not submitted
            to the HTCondor scheduler. The default is False.
        """
        # Collect the job context variables.
        context = {
            'timestamp': time.strftime('%a %b %d %H:%M:%S %Z %Y'),
            'CMSSW_VERSION': os.environ['CMSSW_VERSION'],
            'SCRAM_ARCH': os.environ['SCRAM_ARCH'],
            'input_files': ['preprocess.py'] + [os.path.basename(_) for _ in self.input_files],
            'commands': commands,
            'urls': xrootd_find_files(src),
            'destination': dst,
            'signal_region': self.signal_region,
            'new_branches': self.new_branches.copy(),
            'drop_branches': self.drop_branches,
        }
        # Finalize the job context variables that depend on submission arguments.
        if lumi_scale:
            context['new_branches']['weight/F'] += ' * {0!s}'.format(lumi_scale)
        if scale_factor:
            context['new_branches']['weight/F'] += ' * ({0})'.format(scale_factor)
        context['new_branches']['bin/I'] = self._format_bin_index_formula()
        context['new_branches']['is_signal/I'] = '1' if is_signal else '0'
        # Create the directory tree for the job submission files.
        dagdir = os.path.join(os.getcwd(), 'PreprocessingJobs', name)
        dag_path = os.path.join(dagdir, 'dag')
        dag_exists = True if os.path.isfile(dag_path) else False
        if not dag_exists:
            logdir = os.path.join(dagdir, 'logs')
            safe_makedirs(logdir)
            # Generate the job submission files.
            self._generate_from_template('dag_input_file', dag_path, context)
            self._generate_from_template('node_submit_description', os.path.join(dagdir, 'node'), context)
            self._generate_from_template('run_preprocessor.sh', os.path.join(dagdir, 'run_preprocessor.sh'), context)
            self._generate_from_template('preprocess.py', os.path.join(dagdir, 'preprocess.py'), context)
            for input_file in self.input_files:
                shutil.copy(input_file, dagdir)
        # Unless otherwise directed, submit the DAG input file to DAGMan.
        if self.no_submit or no_submit:
            if dag_exists:
                print 'HTCondor DAG input file exists but not submitted: {0}'.format(dag_path)
            else:
                print 'HTCondor DAG input file generated but not submitted: {0}'.format(dag_path)
        else:
            pass
            xrootd_makedirs(dst)
            subprocess.check_call(['condor_submit_dag', '-usedagdir', '-maxjobs', str(max_jobs), dag_path])

