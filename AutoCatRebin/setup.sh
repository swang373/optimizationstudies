#!/usr/bin/env bash

pushd autocategorizer
git clone https://github.com/acarnes/bdt.git
pushd bdt
git checkout binned_categorizer
popd
make
popd

