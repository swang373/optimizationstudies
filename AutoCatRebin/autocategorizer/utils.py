import collections
import contextlib
import glob
import math
import os
from xml.etree import ElementTree

import ROOT
import jinja2
import numpy


MODULE_DIR = os.path.abspath(os.path.dirname(__file__))

TMPDIR = os.path.join(MODULE_DIR, 'tmp')

TEMPLATE_DIR = os.path.join(MODULE_DIR, 'templates')

JINJA_ENV = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR), trim_blocks=True)


# Clean up dynamically generated build products upon import.
for pattern in ('*.cxx', '*.d', '*.pcm'):
    for match in glob.glob(os.path.join(TMPDIR, pattern)):
        os.remove(match)


@contextlib.contextmanager
def open_root(path):
    """A context manager for handling ROOT files."""
    f = ROOT.TFile.Open(path)
    try:
        yield f
    finally:
        f.Close()


def loadEventsNano(events, features, src, debug=False):
    """Load events from a NanoAOD ntuple."""
    # We use the memory address of the features argument
    # because it is guaranteed unique during its lifetime. 
    funcname = 'loadEventsNano_{0!s}'.format(id(features))
    func = globals().get(funcname, None)
    if func is None:
        with open_root(src) as f:
            t = f.Get('Events')
            branchtypes = collections.OrderedDict()
            for feature in features:
                branchtypes[feature] = t.GetBranch(feature).GetListOfLeaves()[0].GetTypeName()
        macro_source = JINJA_ENV.get_template('loadEventsNano').render(funcname=funcname, branchtypes=branchtypes)
        if debug:
            print macro_source
        macro_path = os.path.join(TMPDIR, '{0}.cxx'.format(funcname))
        with open(macro_path, 'wb') as f:
            f.write(macro_source)
        ROOT.gSystem.CompileMacro(macro_path, 'fOs')
        func = getattr(ROOT, funcname)
        globals()[funcname] = func
    return func(events, features, src)


def parse_rebinning_tree(path):
    """Parse the autocategorizer XML output file for rebinning information.
    """
    bin_significance_squared = []
    bin_edges = []
    
    def recursive_search(node):
        if len(node) > 0:
            bin_edges.append(float(node.get('splitVal')))
            for child in node:
                recursive_search(child)
        else:
            bin_significance_squared.append(float(node.get('significanceSquared')))
    
    xmltree = ElementTree.parse(path)
    recursive_search(xmltree.getroot())
    
    rebinning_info = {
        'bin_edges': numpy.array(sorted(set(bin_edges)), dtype=numpy.float32),
        'total_significance': math.sqrt(sum(bin_significance_squared)),
    }
    
    return rebinning_info

