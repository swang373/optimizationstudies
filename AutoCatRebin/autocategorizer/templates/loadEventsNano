#include <iostream>
#include <string>
#include <vector>

#include "TFile.h"
#include "TTree.h"

#include "Event.h"


////////////////////////////////////////////////////////////////////////////////
/// Load the events to categorize from a NanoAOD ntuple.

void {{ funcname }}(std::vector<Event*>& events, std::vector<std::string>& features, const char* src) {
  TFile* f = TFile::Open(src);
  TTree* tree = (TTree*) f->Get("Events");

  std::cout << "  /// Loading training events from " << src << std::endl;

  Int_t bin, is_signal;
  tree->SetBranchAddress("bin", &bin);
  tree->SetBranchAddress("is_signal", &is_signal);

  Float_t weight;
  tree->SetBranchAddress("weight", &weight);

  {% for branchname, typename in branchtypes.iteritems() %}
  {{ typename }} {{ branchname|upper }} = -999;
  tree->SetBranchAddress("{{ branchname }}", &{{ branchname|upper }});
  {% endfor %}

  for (Long64_t i = 0; i < tree->GetEntries(); i++) {

    tree->GetEntry(i);

    if (weight > -5) {
      Event* event = new Event();
      event->id = i;
      event->bin = bin;
      event->trueValue = is_signal;
      event->weight = weight;

      // Fill the feature vector, reserving the first element for the target.
      event->data = std::vector<double>();
      event->data.push_back(0);
      {% for branchname in branchtypes %}
      event->data.push_back({{ branchname|upper }});
      {% endfor %}

      // Load the event into the vector of training events.
      events.push_back(event);
    }

    // Reset the feature branch addresses to the default value.
    {% for branchname in branchtypes %}
    {{ branchname|upper }} = -999;
    {% endfor %}
  }

  delete f;
}

